//
//  ViewController.swift
//  Topic5
//
//  Created by Matthew Ferguson on 3/14/17.
//  Copyright © 2017 MobileSandbox. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let queue = OperationQueue()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        
        self.launchLoginNav(self)
        
        
        
        #if(false)
    
        ///Transition from the root view controller and Main.Storyboard to the
        /// Login Module.
        switch UIDevice.current.userInterfaceIdiom
        {
            
            case .phone:
            self.LaunchLoginNavPhone(self)
            break
            
        case .pad:
            //self.launchLoginNavPad()
            break
            
        case .unspecified:
            break
            
        default:
            break
        }

        #endif
        
    }

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func launchLoginNav(_ sender:AnyObject)
    {
        
        let operation1 = BlockOperation(block: {
            
            OperationQueue.main.addOperation({
                self.performSegue(withIdentifier: "Main_Root_Segue", sender: self)
            })
            
        })
        
        operation1.completionBlock = {
            #if true
                print("completed LaunchLoginNavPhone")
            #endif
        }
        
        queue.addOperation(operation1)
        
    }



}

